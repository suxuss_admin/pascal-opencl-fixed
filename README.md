修复说明：
原始版本由于是7版本转换至xe2，在很多东西存在问题，所以有了这个版本，除OpenGL部分全部在11.1下测试通过，具体见修复日志。

修改日志：
2024-5-9
1、修复部分类型定义在x64下不正确。
2、移除自带的日志功能并提供FOnLog用于注册日志回调。
3、移除对Windows单元的依赖。
2024-5-10
1、重构Execute方法，现在Execute方法支持event相关功能。
2、重构WriteBuffer和ReadBuffer方法，现在同样支持event相关功能。
2、增加ClEvent相关方法。
3、移除ExecuteTime成员，获取执行时间请使用GetEventRuningTime(Execute)
4、由于GL部分不可用，注释所有OpenGL相关功能。
5、解决打印日志字符串异常。
2024-5-11
1、修复TDCLProgram保存Binary异常。
2、增加CreateProgramWithBinary方法，允许直接使用Binary保存的可执行文件而无需编译。
3、修复CL.pas中clCreateProgramWithBinary一处翻译错误。
4、CreateProgram方法更名为CreateProgramWithSource。
2024-5-13
1、修复Execute工作组数量赋值不正确。
2、改进ReadImage2D/WriteImage2D封装，现在支持event相关功能。
2024-5-14
1、改善SimpleImageLoader中TImageLoader封装。
2024-5-15
1、适配Version 1.2 版本clCreateFromGLTexture2D更名为clCreateFromGLTexture。
2、适配Version 2.0 版本clCreateCommandQueue更名为clCreateCommandQueueWithProperties。
2024-5-16
1、修复WriteImage2D事件数量设置不正确。
2、新增SimpleBitmap单元。
2024-5-24
1、新增CopyImage2DToBuffer、CopyBufferToImage2D方法。
2、新增(Sync)MapBuffer、(Sync)MapImageUnmapMemObject方法。
3、新增SyncReadBuffer、SyncWriteBuffer、SynReadImage2D、SynWriteImage2D方法。
4、为TDCLCommandQueue所有方法增加overload修饰。
5、大量细节优化调整
2024-5-26
1、增强ReadImage2D、WriteImage2D、MapImage2D、CopyImage2DToBuffer、CopyBufferToImage2D方法。
2024-5-27
1、修复origin填写不正确。
2、新增BarrierWithWaitList方法。
3、新增CopyImage2D方法。
4、调整CreateCommandQueue方法以适应更多情况。
5、移除垃圾代码。

PasOpenCL
=========

PasOpenCL is a fork of 'delphi-opencl' hosted on Google Code (http://code.google.com/p/delphi-opencl/). The code of the fork is kept more or less unchanged, but the structure of the repository has changed a bit. For this reason a different name has been picked.

One prominent change is the absense of the dglOpenGL.pas header, which can be found at the personal fork at:
https://github.com/CWBudde/dglOpenGL
or at the original location at
https://bitbucket.org/saschawillems/dglopengl

Below you can read the information given to the original project.

Original Project
---------------- 

This project is intended to make possible the use of OpenCL in Delphi and Free Pascal (Lazarus). 

Authors: 
niello  Site: http://www.niello.org.ua 

Igroman  Site: http://Igroman14.livejournal.com 

What OpenCL platforms / Operating Systems / Compilers are supported? 

OpenCL Platforms: 
tested on AMD, Nvidia 
if you are running on a different platform, please contact us 
Operating Systems: 
Windows XP, Windows Vista, Windows 7 
Code for Linux added but not tested 
if you are running on a different OS, please contact us 
Compilers: 
Delphi (6, 7, 2010, XE2) 
FPC (2.2.0, 2.4.2) 
if you are running on a different compiler, please contact us 


How to use it? 
* CL, CL_Platforms, CL_GL, CL_D3D9, CL_D3D10, CL_D3D11, CL_DX9_Media_Sharing, CL_Ext, CL_GL_Ext (.pas) are OpenCL headers for Pascal compilers. 
* OpenCL (.inc) - is the include file for them. 
* DelphiCL (.pas) - OOP classes for OpenCL. 
* DelphiCL (.inc) - is the include file for the above. (LOGGING for logging in file "DELPHI_LOG.log", PROFILING for profiling). 


Before using the TDCLClasses, need execute procedure: 
   InitOpenCL();// default library name = OpenCL.dll for Windows, libOpenCL.so for Linux
 
 if you are using CL_GL sharing, need added InitCL_GL() procedure: 
   InitCL_GL();
 


The TDCLPlatforms allow get access to the all platforms on the computer (AMD, NVidia, Intel etc.). 
 You need to create specimen of this class: 
 var
 platforms: TDCLPlatforms;
 ...
 begin
   platforms := TDCLPlatforms.Create();
   //Your code here
   FreeAndNil(platforms);
 end;
 


The TDCLPlatforms have property Platforms (array of PDCLPlatform - Pointer to the TDCLPlatform). 
 More in the source files. 
 Maybe later we'll write a more detailed guide.